# Employee Service User-Interface

## Requirements

![](./assets/ui.jpg)

* Angular v9 Single Page Application
* No fancy graphical design is required, use Angular Material and Bootstrap
* Responsive design: rendered properly on desktop and mobile browsers
* Hosted on [Azure Storage](https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blob-static-website)
* End-point specification: https://app.swaggerhub.com/apis/vkhazin/employee-svc/1.0.0
* Use mocks until end-point is ready for integration
* Filter options: 'Last Name', 'First Name', and 'Username'
* Selecting a row will open user details
* User details to be displayed as an overlay on top of the grid

## Development

* Install [Node.js and Npm](https://nodejs.org/en/download/package-manager/)
* Install [Angular cli](https://cli.angular.io/)
* Clone the repository and navigate into the new directory using a terminal
* Install dependencies: `npm install`
* Launch locally: `ng serve`

## Azure Setup

* Log into [Azure Portal](https://portal.azure.com) 
* Proceed to [storage accounts](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Storage%2FStorageAccounts)
* Create a storage account, will take a few moments to complete, navigate to overview when done
* Search for `static website` and enable static hosting
* For `Index document name` type `index.html` and save the changes
* There is also a [video walk-through](https://www.youtube.com/watch?feature=player_embedded&v=SnZ759xn9oM) available

and follow along below video to create a Storage Container > Static Website > $web Container

## Deployment to Azure

* To build for deployment: `ng build --prod`
* To push files to Azure storage:
```
export storage_account=sakpgospocemployeeng
az storage blob upload-batch -d '$web' --account-name ${storage_account} -s ./dist/employees-app
```

